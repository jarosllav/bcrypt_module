#include <sqrat.h>

#include <bcrypt/BCrypt.hpp>

extern "C" SQRESULT SQRAT_API sqmodule_load(HSQUIRRELVM vm, HSQAPI api)
{
	SqModule::Initialize(vm, api);
	Sqrat::DefaultVM::Set(vm);

	Sqrat::RootTable(vm).Func("bcrypt_validatePassword", &BCrypt::validatePassword);
	Sqrat::RootTable(vm).Func("bcrypt_generateHash", &BCrypt::generateHash);

	return SQ_OK;
}
